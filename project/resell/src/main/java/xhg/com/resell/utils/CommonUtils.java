package xhg.com.resell.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dyd
 */
public class CommonUtils {
    private static Context context;
    private static Toast sToast;
    public static final String TIME_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String TIME_YYYY_MM_DD_HH_MM_SS= "yyyy-MM-dd HH:mm:ss";

    /**
     * 在application中初始化
     * @param application
     */
    public static void init(Context application) {
        context = application;
    }

    /**
     * 获取全局context
     * @return
     */
    public static Context getContext() {
        return context;
    }

    /**
     * 土司显示
     * @param msg
     */
    public static void showToast(String msg) {
        if (sToast == null) {
            sToast = Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT);
        }
        sToast.setText(msg);
        sToast.show();
    }

    /**
     * 吐司居中显示
     * @param msg
     */
    public static void showCenterTaost(String msg) {
        if (sToast == null) {
            sToast = Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT);
        }
        sToast.setGravity(Gravity.CENTER, 0, 0);
        sToast.setText(msg);
        sToast.show();
    }

    /**
     * 得到Resources
     * @return
     */
    public static Resources getRescource() {
        return getContext().getResources();
    }

    /**
     * 得到string.xml中字符串，带点为辐
     * @param id
     * @param fromatArgs
     * @return
     */
    public static String getString(int id, Object... fromatArgs) {
        return getRescource().getString(id, fromatArgs);
    }

    /**
     * 得到string.xml中字符串
     * @param resId
     * @return
     */
    public static String getString(int resId) {
        return getRescource().getString(resId);
    }

    /**
     * 得到Drawable对象
     * @param resId
     * @return
     */
    public static Drawable getDrawable(int resId) {
        return getRescource().getDrawable(resId);
    }

    /**
     * 得到color.xml中颜色
     * @param colorId
     * @return
     */
    public static int getColor(int colorId) {
        return getRescource().getColor(colorId);
    }

    /**
     * 得到string.xml中字符数组
     * @param resId
     * @return
     */
    public static String[] getStringArr(int resId) {
        return getRescource().getStringArray(resId);
    }



    /**
     * 得到应用程序包名
     * @return
     */
    public static String getPackageNmae() {
        return getContext().getPackageName();
    }

    /**
     * 得到sn
     */
    public static String getSn() {
        return Build.SERIAL;
    }

    /**
     * 时间戳格式化
     * @param f     格式
     * @param time  时间戳
     * @return      格式化后的时间
     */
    public static String formatTime(String f, long time) {
        SimpleDateFormat format = new SimpleDateFormat(f);
        return format.format(new Date(time));
    }


    /**
     * 获取当前月份
     */
    public static int getMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH) + 1;
    }

    /**
     * 验证是否是手机号码
     *
     * @param str
     * @return
     */
    public static boolean isMobile(String str) {
        Pattern pattern = Pattern.compile("1[0-9]{10}");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}
