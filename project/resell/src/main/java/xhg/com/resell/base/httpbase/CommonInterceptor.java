package xhg.com.resell.base.httpbase;






import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author lew1210
 * 封装公共接口
 */
public class CommonInterceptor implements Interceptor {
    private String versionName;
    public CommonInterceptor(String versionName) {
        this.versionName = versionName;
    }
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request old = chain.request();

        HttpUrl.Builder builder = old.url().newBuilder()
                .scheme(old.url().scheme())
                .host(old.url().host())
                .addQueryParameter("version", versionName);

        Request newBuilder = old.newBuilder()
                .method(old.method(), old.body())
                .url(builder.build())
                .build();
        return chain.proceed(newBuilder);
    }
}
