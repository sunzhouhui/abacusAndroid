package xhg.com.resell.base.activitybase;

import io.reactivex.disposables.Disposable;

public interface BaseCall {
    void onSubscribe(Disposable d);
    void onError(Throwable e);
}
