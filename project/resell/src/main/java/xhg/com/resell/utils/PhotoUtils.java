package xhg.com.resell.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by iii on 2016/11/15.
 * 图片选取工具
 */
public class PhotoUtils {
    public static final int REQUEST_PICK = 1; // 选择图片
    public static final int REQUEST_CAPTURE = 2; // 拍照
    public static final int REQUEST_CROP_PHOTO = 3; // 裁剪图片
    public static final int REQUEST_CROP_PHOTO_OUT = 4; // 裁剪图片

    public static final int CHOOSE_PICTURE = 5;//TODO feiphoto

    public static void showPhotoPicker(final Activity activity, int which) {
        KeyUtils.mUserPhotoName = System.currentTimeMillis() + new Random().nextInt(1000) + ".jpg"; // 初始化用户拍照保存的文件名

        switch (which) {
            case 1://相册
                Intent pickIntent = new Intent(Intent.ACTION_PICK, null);
                pickIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                activity.startActivityForResult(pickIntent, REQUEST_PICK);
                break;

            case 0://相机
                Intent takeIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // 下面这句指定调用相机拍照后的照片存储的路径
                File dir = new File(Environment.getExternalStorageDirectory() + KeyUtils.mPhotoPath);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File imgFile = new File(dir, KeyUtils.mUserPhotoName);
                takeIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imgFile));
                try {
                    activity.startActivityForResult(takeIntent, REQUEST_CAPTURE);
                } catch (Exception e) {
                    CommonUtils.showCenterTaost("设备暂不支持照相");
                }

                break;
            default:
                break;
        }


    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public static void startPhotoZoom(Activity activity, Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        activity.startActivityForResult(intent, REQUEST_CROP_PHOTO);
    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public static void startPhotoZoomOut2(Activity activity, Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        File dir = new File(Environment.getExternalStorageDirectory() + KeyUtils.mPhotoPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File imgFile = new File(dir, KeyUtils.mUserPhotoName);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imgFile));
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        activity.startActivityForResult(intent, REQUEST_CROP_PHOTO_OUT);
    }


    /**
     * 保存图片为文件
     *
     * @param srcPath 图片保存路径
     * @return
     */
    public static boolean saveBitmap2File(Bitmap bitmap, String srcPath) {
        boolean saveSuccess = false;
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(srcPath));
            saveSuccess = bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return saveSuccess;
    }

    /**
     * 按比例大小压缩
     *
     * @param srcPath 图片路径
     * @param kByte   压缩至图片大小上限 ,KB为单位(无效?)
     * @return
     */
    public static Bitmap getBitmap(String srcPath, int kByte) {
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);//此时返回bm为空

        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap, kByte);//压缩好比例大小后再进行质量压缩
    }

    /**
     * 图片质量压缩
     *
     * @param image 压缩的图片
     * @param kByte 压缩至图片大小上限 ,KB为单位(无效?)
     * @return
     */
    private static Bitmap compressImage(Bitmap image, int kByte) {
        int i = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int options = 100;
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        while (baos.toByteArray().length / 1024 > kByte) {    //循环判断如果压缩后图片是否大于50kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            options -= 10;//每次都减少10
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            i++;
        }
//        Logger.d("压缩次数" + i);
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    /**
     * 按比例大小压缩
     *
     * @param image 图片Bitmap
     * @param kByte 压缩至图片大小上限 ,KB为单位(无效?)
     * @return
     */
    public static Bitmap getBitmap(Bitmap image, int kByte) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        //判断如果图片大于1M,进行压缩避免在生成图片
        //（BitmapFactory.decodeStream）时溢出
        if (baos.toByteArray().length / 1024 > 1024) {
            baos.reset();//重置baos即清空baos
            //这里压缩50%，把压缩后的数据存放到baos中
            image.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        return compressImage(bitmap, kByte);//压缩好比例大小后再进行质量压缩
    }

}
