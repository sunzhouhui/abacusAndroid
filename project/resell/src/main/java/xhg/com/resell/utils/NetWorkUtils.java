package xhg.com.resell.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.orhanobut.logger.Logger;

import java.io.IOException;

/**
 * 网络工具
 * @author lew1210
 */
public class NetWorkUtils {

    /**
     * 判断网络是否可用
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }

        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            return false;
        }
        return info.isAvailable();
    }


    /**
     * 判断外网是否有网
     */
    public static boolean ping() {

//        String ip = BuildConfig.UC_LINE;
        String ip = "www.baidu.com";
        try {
            //ping 3次网址
            Process p = Runtime.getRuntime().exec("ping -c 3 -w 100 " + ip);
            int status = p.waitFor();
            Logger.e("status = " + status);
            if (status == 0) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
