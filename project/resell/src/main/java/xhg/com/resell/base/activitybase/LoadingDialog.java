package xhg.com.resell.base.activitybase;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import xhg.com.resell.R;


public class LoadingDialog extends Dialog {
    private ImageView mImageView;
    private AnimationDrawable mAnimation;

    public LoadingDialog(@NonNull Context context) {
        super(context, R.style.CustomDialogStyle);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);
        initView();
        initData();
    }


    private void initView() {
        mImageView = findViewById(R.id.iv_loading);
    }

    private void initData() {
        mAnimation = (AnimationDrawable) mImageView.getBackground();
        mImageView.post(new Runnable() {
            @Override
            public void run() {
                if (!mAnimation.isRunning()) {
                    mAnimation.start();
                }
            }
        });
    }




    @Override
    public void dismiss() {
        super.dismiss();
    }

}
