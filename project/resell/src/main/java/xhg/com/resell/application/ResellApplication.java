package xhg.com.resell.application;


import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import xhg.com.resell.utils.CommonUtils;

public class ResellApplication extends Application {
    public static final String TAG = ResellApplication.class.getSimpleName();
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        initLogger(TAG);
        context = getApplicationContext();
        CommonUtils.init(context);
    }

    /**
     * 日志输出打印
     */
    protected void initLogger(String tag) {
        //对logger初始化
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)  // (Optional) Whether to show thread info or not. Default true
                .methodCount(1)         // (Optional) How many method line to show. Default 2
                .tag(tag)   // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                return Global.DEBUG;
            }
        });
    }
}
