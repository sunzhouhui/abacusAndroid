package xhg.com.resell.base.activitybase;

/**
 * @author lew1210
 * View层基类
 */
public interface BaseView {

    /**
     * 对话框显示
     */
    void showDialog(boolean show);

    /**
     * 显示错误信息
     */
    void error(String msg);


}
