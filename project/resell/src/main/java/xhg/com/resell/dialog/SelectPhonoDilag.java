package xhg.com.resell.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import xhg.com.resell.R;

/**
 * Created by dyd
 * 选择图片弹窗
 */
public class SelectPhonoDilag extends Dialog {
    private Context context;
    ImageView offDialog;
    RelativeLayout mCamera;
    RelativeLayout mPhoto;

    public SelectPhonoDilag(Context context) {
        super(context, R.style.Dialog_select_range);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dilag_select_photo);
        findView();
    }

    private void findView() {
        offDialog = (ImageView) findViewById(R.id.select_dialog_off);
        mCamera = (RelativeLayout) findViewById(R.id.select_camera_re);
        mPhoto = (RelativeLayout) findViewById(R.id.select_photo_re);

        offDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //相机
        mCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPhoto.getSelectPhoto("回调数据", 0);
            }
        });

        //照片
        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPhoto.getSelectPhoto("回调数据", 1);
            }
        });
    }


    /**
     * 回调函数
     */

    public interface SelectPhoto {
        void getSelectPhoto(String demo, int id);
    }

    private SelectPhoto selectPhoto;

    public void setSelectPhoto(SelectPhoto selectPhoto) {
        this.selectPhoto = selectPhoto;
    }
}
