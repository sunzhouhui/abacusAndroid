package xhg.com.resell.base.httpbase;


import io.reactivex.Observable;
import okhttp3.OkHttpClient;

public class BaseHttp {

    protected RetrofitHelper helper;

    public BaseHttp(String baseUrl) {
        helper = RetrofitHelper.getInstance();
        OkHttpClient.Builder builder = helper.getBuilder();
        helper.init(builder, baseUrl);
    }

    public BaseHttp(String baseUrl, String version) {
        helper = RetrofitHelper.getInstance();
        OkHttpClient.Builder builder = helper.getBuilder();
        builder.addInterceptor(new CommonInterceptor(version));
        helper.init(builder, baseUrl);
    }




    public <T> T getApi(Class<T> cls) {
        return  helper.getRetrofit(cls);
    }

    /**
     * 类型转换
     * @param observable
     * @param <T>
     * @return
     */
    public <T> Observable<T> map(Observable<HttpResult<T>> observable) {
        return observable.map(new HttpResultFunc<T>());
    }
}
