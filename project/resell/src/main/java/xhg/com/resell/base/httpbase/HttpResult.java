package xhg.com.resell.base.httpbase;

public class HttpResult<T> extends Result{

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
