package xhg.com.resell.utils;


import java.math.BigDecimal;

public class CalcUtil {
    private static CalcUtil util;
    private CalcUtil(){}

    public static CalcUtil getInstance(){
        if(util ==null) {
            synchronized (CalcUtil.class){
                if(util ==null) {
                    util = new CalcUtil();
                }
            }
        }
        return util;
    }


    /**
     * 对一个数四舍五入保留指定的位数
     */
    public String getRetain(String a,int retain){
        BigDecimal f = new BigDecimal(a);
        BigDecimal result = f.setScale(retain, BigDecimal.ROUND_HALF_UP);
        return String.valueOf(result);
    }

    /**
     * 两数比较大小  返回 1：大于  0：等于  -1：小于
     */
    public int compareTo(String a,String b){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        return f1.compareTo(f2);
    }

    /**
     * 相乘，对结果不做任何其他处理
     */
    public String getMulti(String a,String b){
        BigDecimal f = new BigDecimal(a);
        BigDecimal s = new BigDecimal(b);
        BigDecimal result = f.multiply(s);
        return String.valueOf(result);
    }

    /**
     * 相乘，对结果四舍五入保留指定的位数
     */
    public String getMulti(String a,String b,int retain){
        BigDecimal f = new BigDecimal(a);
        BigDecimal s = new BigDecimal(b);
        BigDecimal result = f.multiply(s).setScale(retain, BigDecimal.ROUND_HALF_UP);
        return String.valueOf(result);
    }


    /**
     * 求和，对结果不做任何处理
     */
    public String add(String a,String b){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result = String.valueOf(f1.add(f2));
        return result;
    }

    /**
     * 求和，对结果保留指定的位数，四舍五入
     */
    public String add(String a,String b,int retain){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result = String.valueOf(f1.add(f2).setScale(retain, BigDecimal.ROUND_HALF_UP));
        return result;
    }

    /**
     * 相减，对结果不做任何处理
     */
    public String sub(String a,String b){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result = String.valueOf(f1.subtract(f2));
        return result;
    }

    /**
     * 相减，对结果保留指定的位数，四舍五入
     */
    public String sub(String a,String b,int retain){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result = String.valueOf(f1.subtract(f2).setScale(retain, BigDecimal.ROUND_HALF_UP));
        return result;
    }


    /**
     * 相除，对结果不做任何处理
     */
    public String divid(String a,String b){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result;
        try{
            result = String.valueOf(f1.divide(f2));
        }catch (ArithmeticException e){//有可能出现无限循环小数，则改称保留10位
            result = String.valueOf(f1.divide(f2,10,BigDecimal.ROUND_HALF_UP));
        }
        return result;
    }

    /**
     * 相除，对结果保留指定的位数，四舍五入
     */
    public String divid(String a,String b,int retain){
        BigDecimal f1 = new BigDecimal(a);
        BigDecimal f2 = new BigDecimal(b);
        String result = String.valueOf(f1.divide(f2,retain,BigDecimal.ROUND_HALF_UP));
        return result;
    }

    public static void main(String[] args) {

        String a = CalcUtil.getInstance().divid("11.3355","11.3354");
        System.out.println(a);

        String b = CalcUtil.getInstance().divid("11.3355","11.3354",3);
        System.out.println(b);

    }


}
