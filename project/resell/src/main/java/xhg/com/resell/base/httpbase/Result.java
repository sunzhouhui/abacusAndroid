package xhg.com.resell.base.httpbase;

/**
 * @author lew1210
 */
public class Result {
    private static final int SUCCESS_CODE = 0;
    private int status = -1;
    private String msg;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return status == SUCCESS_CODE;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
