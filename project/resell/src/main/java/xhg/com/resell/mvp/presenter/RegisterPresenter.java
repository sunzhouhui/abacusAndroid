package xhg.com.resell.mvp.presenter;

import java.util.HashMap;

import io.reactivex.disposables.Disposable;
import xhg.com.resell.mvp.model.RegisterModel;
import xhg.com.resell.mvp.view.IRegisterCons;
import xhg.com.resell.utils.CommonUtils;

public class RegisterPresenter extends IRegisterCons.IRegisterPresenter {
    private RegisterModel model;

    public RegisterPresenter() {
        model = new RegisterModel();
    }

    @Override
    public void getYzm(String user) {
        HashMap map = new HashMap<String, String>();
        map.put("user", user);

        getView().showDialog(true);
        model.getYzm(map, new IRegisterCons.IRegisterSuccessGetYzmCallBack() {
            @Override
            public void onSubscribe(Disposable d) {
                add(d);
            }

            @Override
            public void onError(Throwable e) {
                getView().showDialog(false);
                getView().error(e.getMessage());
            }

            @Override
            public void onSuccessGetYzm() {
                getView().showDialog(false);
            }
        });
    }

    @Override
    public void register(String user, String password, String yzm) {
        if(user.equals("") || user==null){
            getView().error("请输入账号");
            return;
        }else if(!CommonUtils.isMobile(user)){
            getView().error("请输入正确的手机号");
            return;
        }else if(password.equals("") || password==null){
            getView().error("请输入密码");
            return;
        }else if(yzm.equals("") || yzm==null){
            getView().error("请输入验证码");
            return;
        }

        HashMap map = new HashMap<String, String>();
        map.put("user", user);
        map.put("password",password);
        map.put("yzm", yzm);

        getView().showDialog(true);
         model.register(map, new IRegisterCons.IRegisterSuccessRegisterCallBack() {

             @Override
             public void onSubscribe(Disposable d) {
                add(d);
             }

             @Override
             public void onError(Throwable e) {
                 getView().showDialog(false);
                 getView().error(e.getMessage());
             }

             @Override
             public void onSuccessRegister() {
                 getView().showDialog(false);
                 getView().onSuccessRegister();
             }
         });
    }

    @Override
    protected void onDestroy() {

    }
}
