package xhg.com.resell.mvp.presenter;

import java.util.HashMap;

import io.reactivex.disposables.Disposable;
import xhg.com.resell.mvp.model.LoginModel;
import xhg.com.resell.mvp.view.IloginCons;
import xhg.com.resell.utils.CommonUtils;

public class LoginPresenter extends IloginCons.IloginPresenter{

    private LoginModel model;

    public LoginPresenter() {
        model = new LoginModel();
    }

    @Override
    public void login(String user, String password) {
        if(user.equals("") || user==null){
            getView().error("请输入账号");
            return;
        }else if(!CommonUtils.isMobile(user)){
            getView().error("请输入正确的手机号");
            return;
        }else if(password.equals("") || password==null){
            getView().error("请输入密码");
            return;
        }

        HashMap<String, String> map = new HashMap<>();
        map.put("user", user);
        map.put("password", password);

        getView().showDialog(true);
        model.login(map, new IloginCons.IloginCallBack() {
            @Override
            public void onSubscribe(Disposable d) {
                add(d);
            }

            @Override
            public void onError(Throwable e) {
                getView().error(e.getMessage());
                getView().showDialog(false);
            }

            @Override
            public void onSuccessLogin() {
                getView().showDialog(false);
                getView().onSuccessLogin();
            }
        });
    }


    @Override
    protected void onDestroy() {

    }
}
