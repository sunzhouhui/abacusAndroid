package xhg.com.resell.mvp.view;

import xhg.com.resell.base.activitybase.BaseCall;
import xhg.com.resell.base.activitybase.BasePresenter;
import xhg.com.resell.base.activitybase.BaseView;

public interface IRegisterCons {
    interface IRegisterView extends BaseView{
        void onSuccessRegister();
    }

    abstract class IRegisterPresenter extends BasePresenter<IRegisterView> {
        public abstract void getYzm(String user);
        public abstract void register(String user, String password, String yzm);
    }

    interface IRegisterSuccessRegisterCallBack extends BaseCall{
        void onSuccessRegister();
    }

    interface IRegisterSuccessGetYzmCallBack extends BaseCall{
        void onSuccessGetYzm();
    }
}
