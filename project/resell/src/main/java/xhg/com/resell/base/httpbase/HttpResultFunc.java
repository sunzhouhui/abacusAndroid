package xhg.com.resell.base.httpbase;

import io.reactivex.functions.Function;

/**
 * @author lew1210
 */
public class HttpResultFunc<T> implements Function<HttpResult<T>, T> {
    @Override
    public T apply(HttpResult<T> result) throws Exception {
        return result.getData();
    }
}
