package xhg.com.resell.http;


import xhg.com.resell.application.Global;
import xhg.com.resell.base.httpbase.BaseHttp;

/**
 * @author dyd
 * 调用用户中心
 */
public class HttpApiUtils extends BaseHttp {
    private static HttpApiUtils instance;
    private ApiService apiService;

    private HttpApiUtils() {
        super(Global.BASE_URL, Global.VERSION_NAME);
        apiService = getApi(ApiService.class);
    }


    public static HttpApiUtils getInstance() {
        if (instance == null) {
            synchronized (HttpApiUtils.class) {
                if (instance == null) {
                    instance = new HttpApiUtils();
                }
            }
        }

        return instance;
    }


    /**
     * 登录
     */
    /*public Observable<LoginBean> login(HashMap<String, String> map) {
        Observable<HttpResult<LoginBean>> observable = apiService.login(map);
        return map(observable);
    }*/

}
