package xhg.com.resell.activity;

import xhg.com.resell.R;
import xhg.com.resell.base.activitybase.BaseActivity;
import xhg.com.resell.base.activitybase.BasePresenter;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void bindView() {

    }

    @Override
    protected void bindEvent() {

    }

    @Override
    protected String setTitle() {
        return null;
    }
}
