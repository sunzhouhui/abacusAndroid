package xhg.com.resell.mvp.view;

import java.util.HashMap;

import xhg.com.resell.base.activitybase.BaseCall;
import xhg.com.resell.base.activitybase.BasePresenter;
import xhg.com.resell.base.activitybase.BaseView;

// 登陆界面的view
public interface IloginCons {
    interface  IloginView extends BaseView{
        void onSuccessLogin();
    }

   abstract class IloginPresenter extends BasePresenter<IloginView>{
       public abstract void login(String user,String password);
   }

   interface IloginCallBack extends BaseCall{
       void onSuccessLogin();
   }
}
