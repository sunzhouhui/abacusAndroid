package xhg.com.resell.base.activitybase;

/**
 * 适配器item通用点击事件
 * @author lew1210
 * @param <T>
 */
public interface OnItemClickListener<T> {

    /**
     * item
     * @param t 点击回调
     */
    void onClickItem(T t);
}
