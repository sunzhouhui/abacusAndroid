package xhg.com.resell.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import xhg.com.resell.R;

/**
 * 图片加载
 *
 * @author dyd
 */
public class ImageLoad {
    /**
     * 加载网络图片
     * @param context context
     * @param url 图片地址
     * @param iv 要显示图片的控件
     */
    public static void load(Context context, String url, ImageView iv) {
        RequestOptions options = new RequestOptions();
        options.placeholder(R.mipmap.ic_launcher);
        Glide.with(context)
                .load(url)
                .apply(options)
                .into(iv);
    }

    /**
     * 加载图片
     * @param context context
     * @param url 地址
     * @param iv  图片
     * @param placeHolderResId  占位图
     */
    public static void load(Context context, String url, ImageView iv, int placeHolderResId) {
        if (placeHolderResId == -1) {
            Glide.with(context)
                    .load(url)
                    .into(iv);
            return;
        }

        RequestOptions options = new RequestOptions();
        options.placeholder(placeHolderResId);
        Glide.with(context)
                .load(url)
                .apply(options)
                .into(iv);
    }

    /**
     * 圆形图片
     * @param context
     * @param url
     * @param iv
     */
    public static void loadRound(Context context, String url, ImageView iv) {
        RequestOptions options = new RequestOptions();
        options.placeholder(R.mipmap.ic_launcher_round)
                .centerCrop()
                .circleCrop();

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(iv);


    }

}
