package xhg.com.resell;

import android.content.Intent;

import java.util.Timer;
import java.util.TimerTask;

import xhg.com.resell.activity.LoginActivity;
import xhg.com.resell.activity.MainActivity;
import xhg.com.resell.base.activitybase.BaseActivity;
import xhg.com.resell.base.activitybase.BasePresenter;
import xhg.com.resell.utils.KeyUtils;
import xhg.com.resell.utils.SpUtils;

/**
 * 启动页
 */
public class StartActivity extends BaseActivity {

    final Intent intent = new Intent();
    @Override
    protected int getLayoutId() {
        return R.layout.activity_start;
    }

    @Override
    protected void initData() {
        final String user = SpUtils.getString(KeyUtils.USER, "");
        final String password = SpUtils.getString(KeyUtils.PASSWORD,"");
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                action(user,password);
            }
        };
        timer.schedule(timerTask,2*1000);
    }

    private void action(String user,String password) {
        if(!user.equals("") && !password.equals("")){
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
            finish();
        }else{ //去登录/注册
            intent.setClass(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void bindView() {

    }

    @Override
    protected void bindEvent() {

    }

    @Override
    protected String setTitle() {
        return null;
    }
}
