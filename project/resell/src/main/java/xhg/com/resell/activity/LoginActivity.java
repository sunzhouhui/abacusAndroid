package xhg.com.resell.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import xhg.com.resell.R;
import xhg.com.resell.base.activitybase.BaseActivity;
import xhg.com.resell.mvp.presenter.LoginPresenter;
import xhg.com.resell.mvp.view.IloginCons;
import xhg.com.resell.utils.CommonUtils;


/**
 * 登陆页面
 */
public class LoginActivity extends BaseActivity<IloginCons.IloginView, IloginCons.IloginPresenter>
        implements View.OnClickListener, IloginCons.IloginView {

    private EditText et_name;//账号框
    private EditText et_pwd;//密码框
    private Button btn_login;//登录
    private Button btn_register; //注册/修改密码
    private Intent intent = new Intent();
    private String user;
    private String password;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void bindView() {
        et_name = findViewById(R.id.et_name);
        et_pwd = findViewById(R.id.et_pwd);
        btn_login = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_register);
    }

    @Override
    protected IloginCons.IloginPresenter createPresenter() {
        return new LoginPresenter();
    }


    @Override
    protected void initData() {

    }


    @Override
    protected void bindEvent() {
        btn_login.setOnClickListener(this);
        btn_register.setOnClickListener(this);
    }

    @Override
    protected String setTitle() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            user = et_name.getText().toString();
            password = et_pwd.getText().toString();
            mPresenter.login(user, password);
        }
        if (v.getId() == R.id.btn_register) {
            //注册
            intent.setClass(this, RegisterActivity.class);
            startActivity(intent);
        }
    }

    //成功登陆的回调
    @Override
    public void onSuccessLogin() {
        // TODO: 2019/1/20  这个地方要保存账号和密码 ,记得调接口打开注射
        /*SpUtils.putString(KeyUtils.USER,user);
        SpUtils.putString(KeyUtils.PASSWORD,password);*/

        intent.setClass(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showDialog(boolean show) {
        showLoading(show);
    }

    @Override
    public void error(String msg) {
        CommonUtils.showCenterTaost(msg);
    }
}
