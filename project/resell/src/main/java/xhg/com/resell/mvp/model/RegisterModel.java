package xhg.com.resell.mvp.model;

import java.util.HashMap;

import xhg.com.resell.mvp.view.IRegisterCons;

public class RegisterModel {

    public void register(HashMap<String,String> map, IRegisterCons.IRegisterSuccessRegisterCallBack call){
        call.onSuccessRegister();
    }

    public void getYzm(HashMap<String,String> map, IRegisterCons.IRegisterSuccessGetYzmCallBack callBack) {
        callBack.onSuccessGetYzm();
    }
}
