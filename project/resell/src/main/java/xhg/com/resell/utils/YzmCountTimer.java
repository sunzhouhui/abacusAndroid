package xhg.com.resell.utils;

import android.os.CountDownTimer;
import android.widget.TextView;

import xhg.com.resell.R;

/**
 * 获取验证码的倒计时定时器
 */
public class YzmCountTimer extends CountDownTimer {
    private TextView view;

    /**
     * @param millisInFuture  倒计时的总时间，单位为毫秒
     * @param countDownInterval 倒计时的时间间隔，单位为毫秒
     */
    public YzmCountTimer(long millisInFuture, long countDownInterval, TextView view) {
        super(millisInFuture, countDownInterval);
        this.view = view;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        int time = (int) (Math.round((double) millisUntilFinished / 1000) - 1);
        view.setText(String.valueOf(time+"s"));
        view.setClickable(false);
        view.setBackground(CommonUtils.getDrawable(R.drawable.shap_unclick_btn_bg));
        view.setTextColor(CommonUtils.getColor(R.color.ctext_color));
    }

    @Override
    public void onFinish() {
        view.setText("重新发送");
        view.setClickable(true);
        view.setBackground(CommonUtils.getDrawable(R.drawable.shap_btn_bg));
        view.setTextColor(CommonUtils.getColor(R.color.white));
    }
}
