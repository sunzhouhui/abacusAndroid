package xhg.com.resell.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * SharedPreferences工具类
 * @author lew1210
 */
public class SpUtils {
    public static final String CONFIG_FILE_NAME = "config";

    /**
     * 保存Boolean值
     * @param key
     * @param value
     */
    public static void putBoolean(String key, boolean value) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).apply();
    }

    /**
     * 得到Boolean
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(String key, boolean defValue) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    /**
     * 保存String值
     * @param key
     * @param value
     */
    public static void putString(String key, String value) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        sp.edit().putString(key, value).apply();
    }

    /**
     * 得到String
     * @param key
     * @param defValue
     * @return
     */
    public static String getString(String key, String defValue) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        return sp.getString(key, defValue);
    }

    /**
     * 保存int值
     * @param key
     * @param value
     */
    public static void putInt(String key, int value) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).apply();
    }

    /**
     * 得到Int
     * @param key
     * @param defValue
     * @return
     */
    public static int getInt(String key, int defValue) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }

    /**
     * 保存Long
     * @param key
     * @param value
     */
    public static void putLong(String key, long value) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        sp.edit().putLong(key, value).apply();
    }

    /**
     * 得到Long
     * @param key
     * @param defValue
     * @return
     */
    public static long getLong(String key, long defValue) {
        SharedPreferences sp = CommonUtils.getContext().getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        return sp.getLong(key, defValue);
    }
}
