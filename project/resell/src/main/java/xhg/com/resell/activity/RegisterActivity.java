package xhg.com.resell.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import xhg.com.resell.R;
import xhg.com.resell.base.activitybase.BaseActivity;
import xhg.com.resell.mvp.presenter.RegisterPresenter;
import xhg.com.resell.mvp.view.IRegisterCons;
import xhg.com.resell.utils.CommonUtils;
import xhg.com.resell.utils.YzmCountTimer;

/**
 * 注册修改密码页面
 */
public class RegisterActivity extends BaseActivity<IRegisterCons.IRegisterView, IRegisterCons.IRegisterPresenter>
        implements View.OnClickListener, IRegisterCons.IRegisterView {
    private EditText et_name;//账号框
    private EditText et_pwd;//密码框
    private EditText et_show_yzm;//验证码框
    private TextView tv_yzm;//获取验证码的button
    private Button btn_register;//登录
    private YzmCountTimer timer;
    private String user;
    private String password;
    private String yzm;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void bindView() {
        et_name = findViewById(R.id.et_name);
        et_pwd = findViewById(R.id.et_pwd);
        et_show_yzm = findViewById(R.id.et_show_yzm);
        tv_yzm = findViewById(R.id.tv_yzm);
        btn_register = findViewById(R.id.btn_register);
    }

    @Override
    protected IRegisterCons.IRegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void bindEvent() {
        tv_yzm.setOnClickListener(this);
        btn_register.setOnClickListener(this);
    }

    @Override
    protected String setTitle() {
        return null;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tv_yzm) {
            //获取验证码
            user = et_name.getText().toString();
            getYzm();
        }
        if (v.getId() == R.id.btn_register) {
            //注册/修改密码
            user = et_name.getText().toString();
            password = et_pwd.getText().toString();
            yzm = et_show_yzm.getText().toString();
            mPresenter.register(user, password, yzm);
        }
    }

    /**
     * 获取验证码
     */
    private void getYzm() {
        if (user.equals("") || user == null) {
            CommonUtils.showCenterTaost("请输入手机号");
            return;
        } else if (!CommonUtils.isMobile(user)) {
            CommonUtils.showCenterTaost("请输入正确的手机号");
            return;
        }
        if (timer == null) {
            timer = new YzmCountTimer(1000 * 30, 1000, tv_yzm);
        }
        timer.start();

        mPresenter.getYzm(user);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onSuccessRegister() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showDialog(boolean show) {
        showLoading(show);
    }

    @Override
    public void error(String msg) {
        CommonUtils.showCenterTaost(msg);
    }
}
