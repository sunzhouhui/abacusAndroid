package xhg.com.resell.http;

/**
 * @author dyd
 */
public class HttpConstant {

    /**
     * 登录
     */
    public static final String LOGIN_PATH = "user/login";

    /**
     * 获取apptoken
     */
    public static final String APPTOKEN_PATH = "user/apptoken";


}
