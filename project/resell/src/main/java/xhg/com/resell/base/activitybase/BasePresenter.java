package xhg.com.resell.base.activitybase;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * 基础present
 * @author lew1210
 * @param <T>
 */
public abstract class BasePresenter<T> {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    /**
     * 持有ui接口的弱引用
     */
    private WeakReference<T> mViewRef;

    /**
     * 与View进行绑定
     * 在onCreate()中调用
     * @param view
     */
    public void attachView(T view) {
        mViewRef = new WeakReference<>(view);
    }

    /**
     * 得到View
     * @return
     */
    protected T getView() {
        return mViewRef.get();
    }

    /**
     * 判斷是否与view关联
     * @return
     */
    public boolean isViewAttached() {
        return mViewRef != null && getView() != null;
    }

    /**
     * 添加请求
     */
    public void add(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    /**
     * 与view解绑
     * 在ondestroy方法中调用，防止内容溢出
     */
    public void detchView() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }
        mCompositeDisposable.dispose();
        onDestroy();
    }

    /**
     * 释放资源
     */
    protected abstract void onDestroy();
}
