package xhg.com.resell.base.httpbase;

/**
 * @author lew1210
 * 自定义异常
 */
public class ApiException extends RuntimeException {
    private int code;

    public ApiException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
