package com.yingluo.abacus.demo.logic.mvp;


import android.util.Log;

import com.yingluo.abacus.demo.mvp.IMainModle;

public class MainModleImpl implements IMainModle {
    @Override
    public void login(String str) {
        Log.e("dagger2", "=========" +toString()+"|"+str);
    }

    @Override
    public String toString() {
        return "A{test=测试 }";
    }
}
