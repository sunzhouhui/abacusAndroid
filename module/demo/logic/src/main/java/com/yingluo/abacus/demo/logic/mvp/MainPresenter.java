package com.yingluo.abacus.demo.logic.mvp;

import com.yingluo.abacus.demo.logic.app.MainActivity;
import com.yingluo.abacus.demo.mvp.IMainModle;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

@Component(modules = {MainModle.class})
public interface MainPresenter {
    void inject(MainActivity mainPresenter);
}

@Module
class MainModle {
    @Provides
    IMainModle providerMainModle() {
        return new MainModleImpl();
    }

}