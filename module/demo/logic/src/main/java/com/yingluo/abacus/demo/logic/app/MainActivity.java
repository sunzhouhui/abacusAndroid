package com.yingluo.abacus.demo.logic.app;

import android.os.Bundle;

import com.yingluo.abacus.demo.logic.mvp.DaggerMainPresenter;

import butterknife.ButterKnife;

/**
 * 主页面
 */
public class MainActivity extends com.yingluo.abacus.demo.activity.MainActivity  {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMainPresenter.create().inject(this);
        ButterKnife.bind(this);
    }


}
