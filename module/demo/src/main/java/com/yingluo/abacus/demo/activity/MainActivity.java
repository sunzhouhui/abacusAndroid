package com.yingluo.abacus.demo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.yingluo.abacus.demo.R;
import com.yingluo.abacus.demo.R2;
import com.yingluo.abacus.demo.mvp.IMainModle;
import com.yingluo.abacus.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.OnClick;

/**
 * 主页面
 */
public class MainActivity extends Activity {

    @BindString(R2.string.app_name22)
    public String str;
    @Inject
    public IMainModle mainModle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void exit(View view) {
        finish();
    }

    //    public void login(String s, String str) {
//        startActivity(new Intent(this, LoginActivity.class));
//        Log.e("dagger2", "=========" +s+"|"+str);
//    }
    @OnClick(R2.id.test_name)
    public void login() {
        mainModle.login(str);
        startActivity(new Intent(this, LoginActivity.class));
    }

}
